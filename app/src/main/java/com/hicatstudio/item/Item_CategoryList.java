package com.internetfree.item;

public class Item_CategoryList {
   private String Catlist_Pro_CId;
   private String Catlist_Pro_C_Id;
   private String Catlist_Pro_Cate_Image;
   private String Catlist_Pro_Cate_Name;
   private String Catlist_Pro_Cate_Rating;
   private String Catlist_Pro_Conclusion;
   private String Catlist_Pro_Image;
   private String Catlist_Pro_Intro;
   private String Catlist_Pro_Material;
   private String Catlist_Pro_Name;
   private String Catlist_Pro_PId;
   private String Catlist_Pro_Procedure;
   private String Catlist_Pro_Status;

   public String getCatlist_Pro_CId() {
      return this.Catlist_Pro_CId;
   }

   public String getCatlist_Pro_C_Id() {
      return this.Catlist_Pro_C_Id;
   }

   public String getCatlist_Pro_Cate_Image() {
      return this.Catlist_Pro_Cate_Image;
   }

   public String getCatlist_Pro_Cate_Name() {
      return this.Catlist_Pro_Cate_Name;
   }

   public String getCatlist_Pro_Cate_Rating() {
      return this.Catlist_Pro_Cate_Rating;
   }

   public String getCatlist_Pro_Conclusion() {
      return this.Catlist_Pro_Conclusion;
   }

   public String getCatlist_Pro_Image() {
      return this.Catlist_Pro_Image;
   }

   public String getCatlist_Pro_Intro() {
      return this.Catlist_Pro_Intro;
   }

   public String getCatlist_Pro_Material() {
      return this.Catlist_Pro_Material;
   }

   public String getCatlist_Pro_Name() {
      return this.Catlist_Pro_Name;
   }

   public String getCatlist_Pro_PId() {
      return this.Catlist_Pro_PId;
   }

   public String getCatlist_Pro_Procedure() {
      return this.Catlist_Pro_Procedure;
   }

   public String getCatlist_Pro_Status() {
      return this.Catlist_Pro_Status;
   }

   public void setCatlist_Pro_CId(String var1) {
      this.Catlist_Pro_CId = var1;
   }

   public void setCatlist_Pro_C_Id(String var1) {
      this.Catlist_Pro_C_Id = var1;
   }

   public void setCatlist_Pro_Cate_Image(String var1) {
      this.Catlist_Pro_Cate_Image = var1;
   }

   public void setCatlist_Pro_Cate_Name(String var1) {
      this.Catlist_Pro_Cate_Name = var1;
   }

   public void setCatlist_Pro_Cate_Rating(String var1) {
      this.Catlist_Pro_Cate_Rating = var1;
   }

   public void setCatlist_Pro_Conclusion(String var1) {
      this.Catlist_Pro_Conclusion = var1;
   }

   public void setCatlist_Pro_Image(String var1) {
      this.Catlist_Pro_Image = var1;
   }

   public void setCatlist_Pro_Intro(String var1) {
      this.Catlist_Pro_Intro = var1;
   }

   public void setCatlist_Pro_Material(String var1) {
      this.Catlist_Pro_Material = var1;
   }

   public void setCatlist_Pro_Name(String var1) {
      this.Catlist_Pro_Name = var1;
   }

   public void setCatlist_Pro_PId(String var1) {
      this.Catlist_Pro_PId = var1;
   }

   public void setCatlist_Pro_Procedure(String var1) {
      this.Catlist_Pro_Procedure = var1;
   }

   public void setCatlist_Pro_Status(String var1) {
      this.Catlist_Pro_Status = var1;
   }
}
