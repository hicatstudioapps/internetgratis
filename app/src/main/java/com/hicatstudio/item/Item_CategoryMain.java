package com.internetfree.item;

import android.graphics.drawable.Drawable;

public class Item_CategoryMain {
   private int CategoryId;
   private String CategoryName;
   private Drawable ImageUrl;
   private String Rating;

   public int getCategoryId() {
      return this.CategoryId;
   }

   public String getCategoryName() {
      return this.CategoryName;
   }

   public Drawable getImageurl() {
      return this.ImageUrl;
   }

   public String getRating() {
      return this.Rating;
   }

   public void setCategoryId(int var1) {
      this.CategoryId = var1;
   }

   public void setCategoryName(String var1) {
      this.CategoryName = var1;
   }

   public void setImageurl(Drawable var1) {
      this.ImageUrl = var1;
   }

   public void setRating(String var1) {
      this.Rating = var1;
   }
}
