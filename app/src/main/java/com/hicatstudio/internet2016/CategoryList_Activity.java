package com.hicatstudio.internet2016;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.internetfree.adapter.CategoryList_Adapter;
import com.internetfree.item.Item_CategoryList;
import com.internetfree.util.AlertDialogManager;
import com.internetfree.util.Constant;
import com.internetfree.util.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

@SuppressWarnings("ResourceType")
public class CategoryList_Activity extends AppCompatActivity {
    AlertDialogManager alert = new AlertDialogManager();
    String[] allArrayprocatename;
    String[] allArrayprocid;
    String[] allArrayproconclu;
    String[] allArrayproimage;
    String[] allArrayprointro;
    String[] allArraypromate;
    String[] allArrayproname;
    String[] allArraypropid;
    String[] allArrayproproce;
    ArrayList allListprocatename;
    ArrayList allListprocid;
    ArrayList allListproconclu;
    ArrayList allListproimage;
    ArrayList allListprointro;
    ArrayList allListpromate;
    ArrayList allListproname;
    ArrayList allListpropid;
    ArrayList allListproproce;
    ArrayList allListrating;
    String[] allarrayrating;
    ArrayList arrayOfCateList;
    private ArrayList arraylist;
    CategoryList_Adapter categorylistadpter;
    ListView gridView;
    Item_CategoryList itemcatelist;
    private AdView mAdView;
    private Item_CategoryList objAllBean;
    Toolbar toolBar;
    int textlength = 0;

    // $FF: synthetic method
    static ArrayList access$0(CategoryList_Activity var0) {
        return var0.arraylist;
    }

    public void filter(String var1) {
        String var2 = var1.toLowerCase(Locale.getDefault());
        this.arrayOfCateList.clear();
        if (var2.length() == 0) {
            this.arrayOfCateList.addAll(this.arraylist);
        } else {
            Iterator var4 = this.arraylist.iterator();

            while (var4.hasNext()) {
                Item_CategoryList var3 = (Item_CategoryList) var4.next();
                if (var3.getCatlist_Pro_Name().toLowerCase(Locale.getDefault()).contains(var2)) {
                    this.arrayOfCateList.add(var3);
                }
            }
        }

        this.setAdapterToListview();
    }

    protected void onCreate(Bundle var1) {
        super.onCreate(var1);
        this.setContentView(R.layout.categorylist_fragment);
        toolBar=(Toolbar)findViewById(R.id.toolbar);
        toolBar.setTitle(Constant.CATEGORY_NAME);
        final SpannableStringBuilder sb = new SpannableStringBuilder(getString(R.string.app_name));

// Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(Typeface.NORMAL);

// Set the text color for first 4 characters
        sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
        sb.setSpan(bss, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(0.7f),0,getString(R.string.app_name).length(),Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        Typeface font = Typeface.createFromAsset(getAssets(), "gt.ttf");
        sb.setSpan(new CustomTypefaceSpan("" , font), 0 , getString(R.string.app_name).length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        toolBar.setTitle(sb);
        setSupportActionBar(
                toolBar
        );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        this.mAdView = (AdView) this.findViewById(R.id.adView);
        this.mAdView.loadAd((new AdRequest.Builder()).build());
//        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1ba1de")));
//        SpannableString var2 = new SpannableString(Constant.CATEGORYNAME);
//        var2.setSpan(new TypefaceSpan(this, "Kh-Battambang.ttf"), 0, var2.length(), 33);
//        this.getSupportActionBar().setTitle(var2);
        this.gridView = (ListView) this.findViewById(R.id.categorylist_grid);
        this.arrayOfCateList = new ArrayList();
        this.arraylist = new ArrayList();
        this.allListpropid = new ArrayList();
        this.allListprocid = new ArrayList();
        this.allListproname = new ArrayList();
        this.allListproimage = new ArrayList();
        this.allListprointro = new ArrayList();
        this.allListpromate = new ArrayList();
        this.allListproproce = new ArrayList();
        this.allListproconclu = new ArrayList();
        this.allListrating = new ArrayList();
        this.allArraypropid = new String[this.allListpropid.size()];
        this.allArrayprocid = new String[this.allListprocid.size()];
        this.allArrayproname = new String[this.allListproname.size()];
        this.allArrayproimage = new String[this.allListproimage.size()];
        this.allArrayprointro = new String[this.allListprointro.size()];
        this.allArraypromate = new String[this.allListpromate.size()];
        this.allArrayproproce = new String[this.allListproproce.size()];
        this.allArrayproconclu = new String[this.allListproconclu.size()];
        this.allarrayrating = new String[this.allListrating.size()];
        String data="{'Projects_App':[{'project_pid':'27','project_cid':'22','project_name':'internet"+
                "gratis con your"+
                "freedom','project_image':'76897_internet-gratis-anrgentina"+
                "(1).jpg','project_intro':'<p>hoy comparto internet gratis android&nbsp; para Argentina funcionando para este nuevo mes de noviembre 2014, esta configuraci&oacute;n funciona para cualquier operador ya sea claro o movistar, les explicare detalladamente como configurar la aplicaci&oacute;n&nbsp;<strong>your-freedom</strong>&nbsp;y"+
                "tambi&eacute;n una nueva aplicaci&oacute;n llamada&nbsp;<strong>PsiPhon apk</strong>, son f&aacute;cil de usar y no necesitas saber algo de programaci&oacute;n basta con saber manipular el celular,&nbsp;antes de seguir con el tutorial y para no hacer perder tu tiempo valioso te digo que esta configuraci&oacute;n solo sirve para android 4.0+ en adelante y no requiere ser root aunque ser&iacute;a bueno que rootees tu celular para tener m&aacute;s velocidad en el internet free, por otro lado el your-freedom apk solo te da 2 horas diarias para navegar totalmente gratis</p>\r\n','project_materials':'<ul>\r\n <li>Solo sirve para celulares con android versi&oacute;n 4.0+ a mas</li>\r\n  <li>Para usar estas aplicaciones te recomiendo usar una aplicacion ahorrador de bateria</li>\r\n  <li>Funciona con cualquier&nbsp;<strong>APN</strong>&nbsp;(nombre de puntos de acceso)</li>\r\n <li>No tener saldo ni plan de datos.</li>\r\n</ul>\r\n','project_procedure':'<p>Esta"+
                "configuraci&oacute;n ser&aacute; para&nbsp;<strong>your freedom apk</strong>, lee detenidamente y sigue las instrucciones al pie de la letra sobre todo para que no te de errores y luego critiques la aplicaci&oacute;n.</p>\r\n\r\n<ul>\r\n <li>Instalas el your freedom desde el play store.</li>\r\n  <li>Abres el your freedom aceptas si te salta una ventana.</li>\r\n <li>Luego ingresas a configurar</li>\r\n <li>Luego ingresas a conexi&oacute;n de servidor</li>\r\n <li>Luego"+
                "coloca estos par&aacute;metros:\r\n <ul>\r\n    <li>Servidor YF:"+
                "ems23.your-freedom.de</li>\r\n   <li>Modo de conexi&oacute;n:"+
                "DNS</li>\r\n   <li>Puerto: 80 o 443 o 8080</li>\r\n    <li>Ajustes:"+
                "None</li>\r\n    <li>Luego marcas:\r\n   <ul>\r\n      <li>Permitir"+
                "uso de DNS</li>\r\n      <li>Habilitar Cifrado</li>\r\n    </ul>\r\n"+
                "</li>\r\n </ul>\r\n </li>\r\n <li>luego regresas y le das en conectar</li>\r\n</ul>\r\n','project_conclusion':'<p>so es todo como veras es muy f&aacute;cil obtener internet gratis solo hay que saber buscar las aplicaciones correcta si tambi&eacute;n ver que puertos est&aacute;n abiertos en la operadora que usamos, esto fue todo por hoy d&eacute;jame un comentario en la play store as&iacute; sabr&eacute; que es lo que necesitas y tambi&eacute;n si en verdad te funciono el m&eacute;todo, y como sabemos ning&uacute;n m&eacute;todo es eterno si llega a caer y ya no te conecta en un mes pues estar&eacute; subiendo m&aacute;s tutoriales,</p>\r\n','project_status':'1','cid':'22','category_name':'Argentina','category_image':'21031_argentina.png','rating_promedio':'3.3854'}]}";
//        new CategoryList_Activity.MyTask().execute(new String[]{data});
        if (JsonUtils.isNetworkAvailable(this)) {
//            new CategoryList_Activity.MyTask().execute(new String[]{"http://192.168.101.1:8081/internet/d.txt"});
            new CategoryList_Activity.MyTask().execute(new String[]{"http://wildercs.net/wilder/app1/api.php?cat_id=" + Constant.CATEGORYID});
        } else {
            this.showToast(this.getString(R.string.conne_msg1));
            this.alert.showAlertDialog(this, this.getString(R.string.conne_msg2), this.getString(R.string.conne_msg3), Boolean.valueOf(false));
        }

        this.gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
                CategoryList_Activity.this.objAllBean = (Item_CategoryList) CategoryList_Activity.this.arrayOfCateList.get(var3);
                String var7 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_CId();
                String var14 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_PId();
                String var10 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_CId();
                String var15 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Name();
                String var9 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Image();
                String var13 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Intro();
                String var16 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Material();
                String var6 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Procedure();
                String var8 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Conclusion();
                String var11 = CategoryList_Activity.this.objAllBean.getCatlist_Pro_Cate_Rating();
                Intent var12 = new Intent(CategoryList_Activity.this.getApplicationContext(), ProjectDetails_Activity.class);
                var12.putExtra("POSITION", var7);
                var12.putExtra("CATEGORY_ITEM_PID", var14);
                var12.putExtra("CATEGORY_ITEM_PCID", var10);
                var12.putExtra("CATEGORY_ITEM_PNAME", var15);
                var12.putExtra("CATEGORY_ITEM_PIMAGE", var9);
                var12.putExtra("CATEGORY_ITEM_PINTRO", var13);
                var12.putExtra("CATEGORY_ITEM_PMATE", var16);
                var12.putExtra("CATEGORY_ITEM_PPROCE", var6);
                var12.putExtra("CATEGORY_ITEM_PCON", var8);
                var12.putExtra("CATEGORY_ITEM_RATIN", var11);
                CategoryList_Activity.this.startActivity(var12);
                ParseAplication.showInterstitial();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu var1) {
        getMenuInflater().inflate(R.menu.menu_search, var1);
        final SearchView var2 = (SearchView) var1.findItem(R.id.search).getActionView();
        var2.setOnQueryTextFocusChangeListener(new OnFocusChangeListener() {
            // $FF: synthetic field
//         private final MenuItem val$searchMenuItem;
//
//         {
//            this.val$searchMenuItem = var2x;
//         }
//
         public void onFocusChange(View var1, boolean var2x) {
            if(!var2x) {
              // this.val$searchMenuItem.collapseActionView();
               var2.setQuery("", false);
            }

         }
        });
        var2.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String var1) {
                var1 = var1.toString().toLowerCase(Locale.getDefault());
                CategoryList_Activity.this.filter(var1);
                return false;
            }

            public boolean onQueryTextSubmit(String var1) {
                return true;
            }
        });
        return super.onCreateOptionsMenu(var1);
    }


    public boolean onOptionsItemSelected(MenuItem var1) {
        boolean var2;
        switch (var1.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                var2 = true;
                break;
            default:
                var2 = super.onOptionsItemSelected(var1);
        }

        return var2;
    }

    public void setAdapterToListview() {
        this.categorylistadpter = new CategoryList_Adapter(this, R.layout.categorylist_item, this.arrayOfCateList);
        this.gridView.setAdapter(this.categorylistadpter);
    }

    public void showToast(String var1) {
        Toast.makeText(this, var1, 1).show();
    }

    private class MyTask extends AsyncTask<String, Void, String> {
        ProgressDialog pDialog;

        private MyTask() {
        }

        @Override
        protected String doInBackground(String... var1) {
//            return "{'Projects_App':[{'project_pid':'27','project_cid':'22','project_name':'internet"+
//                    "gratis con your"+
//                    "freedom','project_image':'76897_internet-gratis-anrgentina"+
//                    "(1).jpg','project_intro':'<p>hoy comparto internet gratis android&nbsp; para Argentina funcionando para este nuevo mes de noviembre 2014, esta configuraci&oacute;n funciona para cualquier operador ya sea claro o movistar, les explicare detalladamente como configurar la aplicaci&oacute;n&nbsp;<strong>your-freedom</strong>&nbsp;y"+
//                    "tambi&eacute;n una nueva aplicaci&oacute;n llamada&nbsp;<strong>PsiPhon apk</strong>, son f&aacute;cil de usar y no necesitas saber algo de programaci&oacute;n basta con saber manipular el celular,&nbsp;antes de seguir con el tutorial y para no hacer perder tu tiempo valioso te digo que esta configuraci&oacute;n solo sirve para android 4.0+ en adelante y no requiere ser root aunque ser&iacute;a bueno que rootees tu celular para tener m&aacute;s velocidad en el internet free, por otro lado el your-freedom apk solo te da 2 horas diarias para navegar totalmente gratis</p>\r\n','project_materials':'<ul>\r\n <li>Solo sirve para celulares con android versi&oacute;n 4.0+ a mas</li>\r\n  <li>Para usar estas aplicaciones te recomiendo usar una aplicacion ahorrador de bateria</li>\r\n  <li>Funciona con cualquier&nbsp;<strong>APN</strong>&nbsp;(nombre de puntos de acceso)</li>\r\n <li>No tener saldo ni plan de datos.</li>\r\n</ul>\r\n','project_procedure':'<p>Esta"+
//                    "configuraci&oacute;n ser&aacute; para&nbsp;<strong>your freedom apk</strong>, lee detenidamente y sigue las instrucciones al pie de la letra sobre todo para que no te de errores y luego critiques la aplicaci&oacute;n.</p>\r\n\r\n<ul>\r\n <li>Instalas el your freedom desde el play store.</li>\r\n  <li>Abres el your freedom aceptas si te salta una ventana.</li>\r\n <li>Luego ingresas a configurar</li>\r\n <li>Luego ingresas a conexi&oacute;n de servidor</li>\r\n <li>Luego"+
//                    "coloca estos par&aacute;metros:\r\n <ul>\r\n    <li>Servidor YF:"+
//                    "ems23.your-freedom.de</li>\r\n   <li>Modo de conexi&oacute;n:"+
//                    "DNS</li>\r\n   <li>Puerto: 80 o 443 o 8080</li>\r\n    <li>Ajustes:"+
//                    "None</li>\r\n    <li>Luego marcas:\r\n   <ul>\r\n      <li>Permitir"+
//                    "uso de DNS</li>\r\n      <li>Habilitar Cifrado</li>\r\n    </ul>\r\n"+
//                    "</li>\r\n </ul>\r\n </li>\r\n <li>luego regresas y le das en conectar</li>\r\n</ul>\r\n','project_conclusion':'<p>so es todo como veras es muy f&aacute;cil obtener internet gratis solo hay que saber buscar las aplicaciones correcta si tambi&eacute;n ver que puertos est&aacute;n abiertos en la operadora que usamos, esto fue todo por hoy d&eacute;jame un comentario en la play store as&iacute; sabr&eacute; que es lo que necesitas y tambi&eacute;n si en verdad te funciono el m&eacute;todo, y como sabemos ning&uacute;n m&eacute;todo es eterno si llega a caer y ya no te conecta en un mes pues estar&eacute; subiendo m&aacute;s tutoriales,</p>\r\n','project_status':'1','cid':'22','category_name':'Argentina','category_image':'21031_argentina.png','rating_promedio':'3.3854'}]}";
            return JsonUtils.getJSONString(var1[0]);
        }

        protected void onPostExecute(String param1) {
            // $FF: Couldn't be decompiled
            JSONArray s;
            try {
                s = (new JSONObject(param1)).getJSONArray("Projects_App");
                if (pDialog != null && pDialog.isShowing())
                {
                    pDialog.dismiss();
                }
                if (s == null || s.length() == 0)
                {
                    showToast(getString(R.string.conne_msg4));
                    alert.showAlertDialog(CategoryList_Activity.this, getString(R.string.conne_msg4), getString(R.string.conne_msg5), Boolean.valueOf(false));
                    return;
                }
                for (int i = 0; i < s.length(); i++) {
                    JSONObject jsonobject = s.getJSONObject(i);
                    Item_CategoryList item_categorylist = new Item_CategoryList();
                    item_categorylist.setCatlist_Pro_PId(jsonobject.getString("project_pid"));
                    item_categorylist.setCatlist_Pro_Name(jsonobject.getString("project_name"));
                    item_categorylist.setCatlist_Pro_Image(jsonobject.getString("project_image"));
                    item_categorylist.setCatlist_Pro_Intro(jsonobject.getString("project_intro"));
                    item_categorylist.setCatlist_Pro_Material(jsonobject.getString("project_materials"));
                    item_categorylist.setCatlist_Pro_Procedure(jsonobject.getString("project_procedure"));
                    item_categorylist.setCatlist_Pro_Conclusion(jsonobject.getString("project_conclusion"));
                    item_categorylist.setCatlist_Pro_CId(jsonobject.getString("project_cid"));
                    item_categorylist.setCatlist_Pro_Cate_Rating(jsonobject.getString("rating_promedio"));
                    arrayOfCateList.add(item_categorylist);
                }
                arraylist.addAll(arrayOfCateList);
                setAdapterToListview();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(CategoryList_Activity.this);
            this.pDialog.setMessage(CategoryList_Activity.this.getString(R.string.loading));
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }
    }
}
