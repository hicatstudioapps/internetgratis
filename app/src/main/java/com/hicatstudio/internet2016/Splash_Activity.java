package com.hicatstudio.internet2016;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash_Activity extends Activity {
   protected boolean active = true;
   protected int splashTime = 2000;

   protected void onCreate(Bundle var1) {
      this.requestWindowFeature(1);
      super.onCreate(var1);
      this.setContentView(R.layout.splash_fragment);
      new Handler().postDelayed(new Runnable() {
         @Override
         public void run() {
            startActivity(new Intent(getApplicationContext(), CategoryMain_Activity.class));
//            ParseAplication.showInterstitial();
            finish();
         }
      }, 2000);

   }
}
