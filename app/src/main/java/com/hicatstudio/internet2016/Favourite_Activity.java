package com.hicatstudio.internet2016;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.internetfree.adapter.Favourite_Adapter;
import com.internetfree.favorite.DatabaseHandler;
import com.internetfree.favorite.Pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("ResourceType")
public class Favourite_Activity extends AppCompatActivity {
   String[] allArrayprocatename;
   String[] allArrayprocid;
   String[] allArrayproconclu;
   String[] allArrayproimage;
   String[] allArrayprointro;
   String[] allArraypromate;
   String[] allArrayproname;
   String[] allArraypropid;
   String[] allArrayproproce;
   String[] allArrayprorating;
   List allData;
   ArrayList allListprocatename;
   ArrayList allListprocid;
   ArrayList allListproconclu;
   ArrayList allListproimage;
   ArrayList allListprointro;
   ArrayList allListpromate;
   ArrayList allListproname;
   ArrayList allListpropid;
   ArrayList allListproproce;
   ArrayList allListprorating;
   DatabaseHandler db;
   Favourite_Adapter favo_adapter;
   ListView grid_fav;
   private AdView mAdView;
   Pojo pojoitem;
   int textlength = 0;
   TextView txt_no;
    Toolbar toolbar;

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.favourite_fragment);
       toolbar= (Toolbar)findViewById(R.id.toolbar);
      this.mAdView = (AdView)this.findViewById(R.id.adView);
      this.mAdView.loadAd((new AdRequest.Builder()).build());
       setSupportActionBar(toolbar);
      this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       this.getSupportActionBar().setHomeButtonEnabled(true);
      SpannableString var2 = new SpannableString("My Favorite");
      //this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1ba1de")));
      //var2.setSpan(new TypefaceSpan(this, "Kh-Battambang.ttf"), 0, var2.length(), 33);
      this.getSupportActionBar().setTitle(var2);
      this.db = new DatabaseHandler(this.getApplicationContext());
      this.grid_fav = (ListView)this.findViewById(R.id.grid_fav);
      this.txt_no = (TextView)this.findViewById(R.id.textView1);
      this.allData = this.db.getAllData();
      this.favo_adapter = new Favourite_Adapter(this, R.layout.favourite_item, this.allData);
      this.grid_fav.setAdapter(this.favo_adapter);
      if(this.allData.size() == 0) {
         this.txt_no.setVisibility(0);
      } else {
         this.txt_no.setVisibility(4);
      }

      this.grid_fav.setOnItemClickListener(new OnItemClickListener() {
         public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
            Favourite_Activity.this.pojoitem = (Pojo)Favourite_Activity.this.allData.get(var3);
            String var14 = Favourite_Activity.this.pojoitem.getCatlist_Pro_CId();
            String var15 = Favourite_Activity.this.pojoitem.getCatlist_Pro_PId();
            String var11 = Favourite_Activity.this.pojoitem.getCatlist_Pro_CId();
            String var13 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Name();
            String var16 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Image();
            String var8 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Intro();
            String var10 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Material();
            String var12 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Procedure();
            String var9 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Conclusion();
            String var6 = Favourite_Activity.this.pojoitem.getCatlist_Pro_Rating();
            Intent var7 = new Intent(Favourite_Activity.this.getApplicationContext(), ProjectDetails_Activity.class);
            var7.putExtra("POSITION", var14);
            var7.putExtra("CATEGORY_ITEM_PID", var15);
            var7.putExtra("CATEGORY_ITEM_PCID", var11);
            var7.putExtra("CATEGORY_ITEM_PNAME", var13);
            var7.putExtra("CATEGORY_ITEM_PIMAGE", var16);
            var7.putExtra("CATEGORY_ITEM_PINTRO", var8);
            var7.putExtra("CATEGORY_ITEM_PMATE", var10);
            var7.putExtra("CATEGORY_ITEM_PPROCE", var12);
            var7.putExtra("CATEGORY_ITEM_PCON", var9);
            var7.putExtra("CATEGORY_ITEM_RATIN", var6);
            Favourite_Activity.this.startActivity(var7);
         }
      });
   }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        final SearchView var2 = (SearchView)menu.findItem(R.id.search).getActionView();
        var2.setOnQueryTextFocusChangeListener(new OnFocusChangeListener() {
                public void onFocusChange(View var1, boolean var2x) {
//                if(!var2x) {
//                    this.val$searchMenuItem.collapseActionView();
//                    var2.setQuery("", false);
//                }

            }
        });
        var2.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String var1) {
                var1 = var1.toString().toLowerCase(Locale.getDefault());
                Favourite_Activity.this.favo_adapter.filter(var1);
                return false;
            }

            public boolean onQueryTextSubmit(String var1) {
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      switch(var1.getItemId()) {
      case 16908332:
         this.onBackPressed();
         var2 = true;
         break;
      default:
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   protected void onResume() {
      super.onResume();
      this.allData = this.db.getAllData();
      this.favo_adapter = new Favourite_Adapter(this, R.layout.favourite_item, this.allData);
      this.grid_fav.setAdapter(this.favo_adapter);
      if(this.allData.size() == 0) {
         this.txt_no.setVisibility(0);
      } else {
         this.txt_no.setVisibility(4);
      }

      this.allListpropid = new ArrayList();
      this.allListprocid = new ArrayList();
      this.allListproname = new ArrayList();
      this.allListproimage = new ArrayList();
      this.allListprointro = new ArrayList();
      this.allListpromate = new ArrayList();
      this.allListproproce = new ArrayList();
      this.allListproconclu = new ArrayList();
      this.allListprorating = new ArrayList();
      this.allArraypropid = new String[this.allListpropid.size()];
      this.allArrayprocid = new String[this.allListprocid.size()];
      this.allArrayproname = new String[this.allListproname.size()];
      this.allArrayproimage = new String[this.allListproimage.size()];
      this.allArrayprointro = new String[this.allListprointro.size()];
      this.allArraypromate = new String[this.allListpromate.size()];
      this.allArrayproproce = new String[this.allListproproce.size()];
      this.allArrayproconclu = new String[this.allListproconclu.size()];
      this.allArrayprorating = new String[this.allListprorating.size()];
   }
}
