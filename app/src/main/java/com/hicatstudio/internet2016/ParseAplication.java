package com.hicatstudio.internet2016;

import android.app.Application;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onesignal.OneSignal;
//import com.parse.Parse;
//import com.parse.ParseInstallation;
//import com.parse.PushService;
//import org.wmtech.internetgratisandroidd.CategoryMain_Activity;

public class ParseAplication extends Application {

   public static InterstitialAd interstitialAd;
   AdRequest request;

   public void onCreate() {
      super.onCreate();
      OneSignal.startInit(this).init();
      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
      ImageLoader.getInstance().init(config);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId(getString(R.string.admob_publisher_interstitial_id));
      request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
         @Override
         public void onAdClosed() {
            interstitialAd.loadAd(request);
         }
      });
      interstitialAd.loadAd(request);
//      Parse.initialize(this, this.getString(R.string.parse_application_id), this.getString(R.string.parse_client_id));
//      PushService.setDefaultPushCallback(this, CategoryMain_Activity.class);
//      ParseInstallation.getCurrentInstallation().saveInBackground();
//      PushService.subscribe(this.getBaseContext(), "InternetGratis", CategoryMain_Activity.class);
   }
   public static void showInterstitial(){
      if(interstitialAd.isLoaded()){
         interstitialAd.show();
      }
   }
}
