package com.hicatstudio.internet2016;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.internetfree.adapter.CategoryMain_Adapter;
import com.internetfree.item.Item_CategoryMain;
import com.internetfree.util.AlertDialogManager;
import com.internetfree.util.Constant;
import com.internetfree.util.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import badabing.lib.ServerUtilities;
import badabing.lib.apprater.AppRater;

public class CategoryMain_Activity extends AppCompatActivity {
   AlertDialogManager alert = new AlertDialogManager();
   ArrayList arrayOfCategory;
   CategoryMain_Adapter categorydAdapter;
   ListView gridView;
   ImageView imgfavourite;
   ImageView imglike;
   Item_CategoryMain item;
   private AdView mAdView;
   private InterstitialAd mInterstitial;
   private Toolbar toolBar;
   TextView txttitle;
   Context context;

   @Override
   protected void onResume() {
      super.onResume();
//      new CategoryMain_Activity.MyTask().execute(new String[]{"http://wildercs.net/wilder/app1/api.php"});
      if(JsonUtils.isNetworkAvailable(this) ) {
//         new CategoryMain_Activity.MyTask().execute(new String[]{"http://wildercs.net/wilder/app1/api.php"});
         if(categorydAdapter == null)
         new CategoryMain_Activity.MyTask().execute(new String[]{"http://wildercs.net/wilder/app1/api.php"});
      } else {
         this.showToast(this.getString(R.string.conne_msg1));
         this.alert.showAlertDialog(this, this.getString(R.string.conne_msg2), this.getString(R.string.conne_msg3), Boolean.valueOf(false));
      }
   }

   protected void onCreate(Bundle var1) {
      this.requestWindowFeature(1);
      super.onCreate(var1);
      SharedPreferences sp=getSharedPreferences("update",MODE_PRIVATE);
//      if(!sp.getBoolean("service",false)){
//
//      }
      startService(new Intent(this, UpdateServices.class));
      context=this;
      this.setContentView(R.layout.categorymain_fragment);
      toolBar=(Toolbar)findViewById(R.id.toolbar);
      toolBar.setTitle(getString(R.string.app_name));
      toolBar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_launcher));
      final SpannableStringBuilder sb = new SpannableStringBuilder(getString(R.string.app_name));

// Span to set text color to some RGB value
      final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
      final StyleSpan bss = new StyleSpan(Typeface.NORMAL);

// Set the text color for first 4 characters
      sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
      sb.setSpan(bss, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      sb.setSpan(new RelativeSizeSpan(0.7f),0,getString(R.string.app_name).length(),Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      Typeface font = Typeface.createFromAsset(getAssets(), "gt.ttf");
      sb.setSpan(new CustomTypefaceSpan("" , font), 0 , getString(R.string.app_name).length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      toolBar.setTitle(sb);
      setSupportActionBar(toolBar);
      this.mAdView = (AdView)this.findViewById(R.id.adView);
      this.mAdView.loadAd((new AdRequest.Builder()).build());
      this.mAdView.setAdListener(new AdListener() {
         @Override
         public void onAdLoaded() {
            super.onAdLoaded();
            mAdView.setVisibility(View.VISIBLE);
         }
      });

      this.gridView = (ListView)this.findViewById(R.id.category_gridview);

      this.arrayOfCategory = new ArrayList();


      this.gridView.setOnItemClickListener(new OnItemClickListener() {
         public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
            CategoryMain_Activity.this.item = (Item_CategoryMain)CategoryMain_Activity.this.arrayOfCategory.get(var3);
            Constant.CATEGORYID = CategoryMain_Activity.this.item.getCategoryId();
            Constant.CATEGORYNAME = CategoryMain_Activity.this.item.getCategoryName();
            Intent var6 = new Intent(CategoryMain_Activity.this.getApplicationContext(), CategoryList_Activity.class);
            CategoryMain_Activity.this.startActivity(var6);
         }
      });
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.main, var1);
      return true;
   }

   public boolean onKeyDown(int var1, KeyEvent var2) {
      boolean var3;
      if(var1 == 4) {
         Builder var4 = new Builder(this);
         var4.setTitle(R.string.app_name);
         var4.setIcon(R.mipmap.ic_launcher);
         var4.setMessage("Seguro Que Quieres Salir?");
         var4.setPositiveButton("Si", new android.content.DialogInterface.OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
              ParseAplication.showInterstitial();
              CategoryMain_Activity.this.finish();
            }
         });
         var4.setNegativeButton("Rate App", new android.content.DialogInterface.OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               AppRater.rateNow(context);
            }
         });
         var4.show();
         var3 = true;
      } else {
         var3 = super.onKeyDown(var1, var2);
      }
      return var3;
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var4;
      switch (var1.getItemId()){
         case R.id.img_favo:
            Intent var2 = new Intent(CategoryMain_Activity.this.getApplicationContext(), Favourite_Activity.class);
            CategoryMain_Activity.this.startActivity(var2);
            break;
         case R.id.img_like:
            AppRater.rateNow(this);
            break;
      }
      return true;
   }

   public void onStart() {
      super.onStart();
      //EasyTracker.getInstance(this).activityStart(this);
   }

   public void onStop() {
      super.onStop();
     // EasyTracker.getInstance(this).activityStop(this);
   }

   public void setAdapterToListview() {
      this.categorydAdapter = new CategoryMain_Adapter(this, R.layout.categoymain_item, this.arrayOfCategory);
      this.gridView.setAdapter(this.categorydAdapter);
   }

   public void showToast(String var1) {
      Toast.makeText(this, var1, Toast.LENGTH_LONG).show();
   }

   private class MyTask extends AsyncTask<String,Void,String> {
      ProgressDialog pDialog;

      private MyTask() {
      }

      // $FF: synthetic method
      MyTask(CategoryMain_Activity.MyTask var2) {
         this();
      }

      protected String doInBackground(String... var1) {
         return JsonUtils.getJSONString(var1[0]);
//            return "dd";
      }

      protected void onPostExecute(String param1) {
         try {
            if(param1==null)
               return;
            String data="{'Projects_App':[{'cid':'22','category_name':'Argentina','category_image':'21031_argentina.png'},{'cid':'20','category_name':'Chile','category_image':'5091_chile.png'},{'cid':'19','category_name':'Colombia','category_image':'22203_Colombia.jpg'},{'cid':'18','category_name':'Ecuador','category_image':'54918_ecuador.jpg'},{'cid':'17','category_name':'Guatemala','category_image':'26908_guatemala.jpg'},{'cid':'16','category_name':'M\\u00e9xico','category_image':'36851_mexico.jpg'},{'cid':'15','category_name':'Nicaragua','category_image':'45353_nicaragua.png'},{'cid':'14','category_name':'Per\\u00fa','category_image':'48036_peru.jpg'},{'cid':'13','category_name':'United States','category_image':'62338_american-flag-graphic.png'},{'cid':'12','category_name':'Otros','category_image':'88627_paises.jpg'}]}";
            JSONArray tuts = (new JSONObject(param1)).getJSONArray("Projects_App");
            if(tuts.length()>0){
               for (int i = 0; i < tuts.length(); i++) {
                  JSONObject jsonobject = tuts.getJSONObject(i);
                  Item_CategoryMain item_categorymain = new Item_CategoryMain();
                  item_categorymain.setCategoryId(jsonobject.getInt("cid"));
                  item_categorymain.setCategoryName(jsonobject.getString("category_name"));
                  if(item_categorymain.getCategoryName().equals("Argentina")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.argentina));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }
                  if(item_categorymain.getCategoryName().equals("Chile")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.chile));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Colombia")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.colombia));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Ecuador")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.ecuador));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Guatemala")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.guatemala));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("México")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.mexico));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Nicaragua")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.nicaragua));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Perú")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.peru));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("UnitedStates")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.eu));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }if(item_categorymain.getCategoryName().equals("Otros")){
                     item_categorymain.setImageurl(getResources().getDrawable(R.drawable.otros));
                     arrayOfCategory.add(item_categorymain);
                     continue;
                  }
               }
               setAdapterToListview();
               if(pDialog.isShowing())
                  pDialog.dismiss();
            }
         } catch (Exception e) {
            e.printStackTrace();
         }

      }

      protected void onPreExecute() {
         super.onPreExecute();
         this.pDialog = new ProgressDialog(CategoryMain_Activity.this);
         this.pDialog.setIndeterminate(true);
         this.pDialog.setMessage(CategoryMain_Activity.this.getString(R.string.loading));
         this.pDialog.setCancelable(false);
         this.pDialog.show();
      }
   }
}
