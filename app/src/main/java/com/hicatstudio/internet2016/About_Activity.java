package com.hicatstudio.internet2016;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import java.io.IOException;
import java.io.InputStream;

public class About_Activity extends Activity {
   String summary = "";

   public void onCreate(Bundle var1) {
      this.requestWindowFeature(1);
      super.onCreate(var1);
      this.setContentView(R.layout.about_fragment);
      WebView var5 = new WebView(this);
      this.setContentView(var5);

      try {
         InputStream var3 = this.getAssets().open("index.html");
         byte[] var2 = new byte[var3.available()];
         var3.read(var2);
         var3.close();
         var5.loadData(new String(var2), "text/html", "UTF-8");
      } catch (IOException var4) {
         var4.printStackTrace();
      }

   }
}
