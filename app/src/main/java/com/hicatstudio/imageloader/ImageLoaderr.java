package com.internetfree.imageloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings("ResourceType")
public class ImageLoaderr {
   ExecutorService executorService;
   FileCache fileCache;
   private Map imageViews = Collections.synchronizedMap(new WeakHashMap());
   MemoryCache memoryCache = new MemoryCache();
   final int stub_id = 2130837616;

   public ImageLoaderr(Context var1) {
      this.fileCache = new FileCache(var1);
      this.executorService = Executors.newFixedThreadPool(5);
   }

   private Bitmap decodeFile(File param1) {
      return null;
      // $FF: Couldn't be decompiled
   }

   private Bitmap getBitmap(String var1) {
      File var3 = this.fileCache.getFile(var1);
      Bitmap var2 = this.decodeFile(var3);
      Bitmap var5;
      if(var2 != null) {
         var5 = var2;
      } else {
         try {
            HttpURLConnection var6 = (HttpURLConnection)(new URL(var1)).openConnection();
            var6.setConnectTimeout(30000);
            var6.setReadTimeout(30000);
            var6.setInstanceFollowRedirects(true);
            InputStream var7 = var6.getInputStream();
            FileOutputStream var8 = new FileOutputStream(var3);
            Utils.CopyStream(var7, var8);
            var8.close();
            var5 = this.decodeFile(var3);
         } catch (Exception var4) {
            var4.printStackTrace();
            var5 = null;
         }
      }

      return var5;
   }

   private void queuePhoto(String var1, ImageView var2) {
      ImageLoaderr.PhotoToLoad var3 = new ImageLoaderr.PhotoToLoad(var1, var2);
      this.executorService.submit(new ImageLoaderr.PhotosLoader(var3));
   }

   public void DisplayImage(String var1, ImageView var2) {
      this.imageViews.put(var2, var1);
      Bitmap var3 = this.memoryCache.get(var1);
      if(var3 != null) {
         var2.setImageBitmap(var3);
      } else {
         this.queuePhoto(var1, var2);
         var2.setImageResource(2130837616);
      }

   }

   public void clearCache() {
      this.memoryCache.clear();
      this.fileCache.clear();
   }

   boolean imageViewReused(ImageLoaderr.PhotoToLoad var1) {
      String var2 = (String)this.imageViews.get(var1.imageView);
      boolean var3;
      if(var2 != null && var2.equals(var1.url)) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   class BitmapDisplayer implements Runnable {
      Bitmap bitmap;
      ImageLoaderr.PhotoToLoad photoToLoad;

      public BitmapDisplayer(Bitmap var2, ImageLoaderr.PhotoToLoad var3) {
         this.bitmap = var2;
         this.photoToLoad = var3;
      }

      public void run() {
         if(!ImageLoaderr.this.imageViewReused(this.photoToLoad) && this.bitmap != null) {
            this.photoToLoad.imageView.setImageBitmap(this.bitmap);
         }

      }
   }

   private class PhotoToLoad {
      public ImageView imageView;
      public String url;

      public PhotoToLoad(String var2, ImageView var3) {
         this.url = var2;
         this.imageView = var3;
      }
   }

   class PhotosLoader implements Runnable {
      ImageLoaderr.PhotoToLoad photoToLoad;

      PhotosLoader(ImageLoaderr.PhotoToLoad var2) {
         this.photoToLoad = var2;
      }

      public void run() {
         if(!ImageLoaderr.this.imageViewReused(this.photoToLoad)) {
            Bitmap var1 = ImageLoaderr.this.getBitmap(this.photoToLoad.url);
            ImageLoaderr.this.memoryCache.put(this.photoToLoad.url, var1);
            if(!ImageLoaderr.this.imageViewReused(this.photoToLoad)) {
               ImageLoaderr.BitmapDisplayer var2 = ImageLoaderr.this.new BitmapDisplayer(var1, this.photoToLoad);
               ((Activity)this.photoToLoad.imageView.getContext()).runOnUiThread(var2);
            }
         }

      }
   }
}
