package com.internetfree.imageloader;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public class MemoryCache {
   private HashMap cache = new HashMap();

   public void clear() {
      this.cache.clear();
   }

   public Bitmap get(String var1) {
      Bitmap var2;
      if(!this.cache.containsKey(var1)) {
         var2 = null;
      } else {
         var2 = (Bitmap)((SoftReference)this.cache.get(var1)).get();
      }

      return var2;
   }

   public void put(String var1, Bitmap var2) {
      this.cache.put(var1, new SoftReference(var2));
   }
}
