package com.internetfree.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;

import com.hicatstudio.internet2016.R;

public class AlertDialogManager {

   public void showAlertDialog(Context var1, String var2, String var3, Boolean var4) {
      AlertDialog var6 = (new Builder(var1)).create();
      var6.setTitle(var2);
      var6.setMessage(var3);
      if(var4 != null) {
         Drawable var5;
         if(var4.booleanValue()) {
            var5 = var1.getResources().getDrawable(R.drawable.success);
         } else {
            var5 = var1.getResources().getDrawable(R.drawable.fail);
         }

         var6.setIcon(var5);
      }

      var6.setButton("OK", new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
         }
      });
      var6.show();
   }
}
