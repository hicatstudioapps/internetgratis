package com.internetfree.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SquareImageViewMainCate extends ImageView {
   public SquareImageViewMainCate(Context var1) {
      super(var1);
   }

   public SquareImageViewMainCate(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public SquareImageViewMainCate(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   protected void onMeasure(int var1, int var2) {
      super.onMeasure(var1, var2);
      this.setMeasuredDimension(this.getMeasuredWidth(), (int)((double)this.getMeasuredWidth() / 1.2D));
   }
}
