package com.internetfree.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

public class TypefaceSpan extends MetricAffectingSpan {
   private static LruCache sTypefaceCache = new LruCache(12);
   private Typeface mTypeface;

   public TypefaceSpan(Context var1, String var2) {
      this.mTypeface = (Typeface)sTypefaceCache.get(var2);
      if(this.mTypeface == null) {
         this.mTypeface = Typeface.createFromAsset(var1.getApplicationContext().getAssets(), String.format("%s", new Object[]{var2}));
         sTypefaceCache.put(var2, this.mTypeface);
      }

   }

   public void updateDrawState(TextPaint var1) {
      var1.setTypeface(this.mTypeface);
      var1.setColor(Color.parseColor("#ffffff"));
      var1.setFlags(var1.getFlags() | 128);
   }

   public void updateMeasureState(TextPaint var1) {
      var1.setTypeface(this.mTypeface);
      var1.setFlags(var1.getFlags() | 128);
   }
}
