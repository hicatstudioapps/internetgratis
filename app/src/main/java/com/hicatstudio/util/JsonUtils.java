package com.internetfree.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.view.Display;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@SuppressWarnings("ResourceType")
public class JsonUtils {
   private Context _context;

   public JsonUtils(Context var1) {
      this._context = var1;
   }

   public static String getJSONString(String param0) {
      try {
         return get(param0);
      } catch (IOException e) {
         e.printStackTrace();
      }
      return null;
//      return "{'Projects_App':[{'cid':'22','category_name':'Argentina','category_image':'21031_argentina.png'},{'cid':'20','category_name':'Chile','category_image':'5091_chile.png'},{'cid':'19','category_name':'Colombia','category_image':'22203_Colombia.jpg'},{'cid':'18','category_name':'Ecuador','category_image':'54918_ecuador.jpg'},{'cid':'17','category_name':'Guatemala','category_image':'26908_guatemala.jpg'},{'cid':'16','category_name':'M\u00e9xico','category_image':'36851_mexico.jpg'},{'cid':'15','category_name':'Nicaragua','category_image':'45353_nicaragua.png'},{'cid':'14','category_name':'Per\\u00fa','category_image':'48036_peru.jpg'},{'cid':'13','category_name':'United" +
//         "States','category_image':'62338_american-flag-graphic.png'},{'cid':'12','category_name':'Otros','category_image':'88627_paises.jpg'}]}";
      // $FF: Couldn't be decompiled
   }

   private static String convertStreamToString(InputStream param0) {
      StringBuilder stringbuilder;
      BufferedReader bufferedreader;
      bufferedreader = new BufferedReader(new InputStreamReader(param0));
      stringbuilder = new StringBuilder();
      String s = null;
      try {
         s = bufferedreader.readLine();
         stringbuilder.append((new StringBuilder()).append(s).append("\n").toString());
         return stringbuilder.toString();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return null;
   }

   public static String get(String var0) throws IOException {
      HttpURLConnection var1 = (HttpURLConnection)(new URL(var0)).openConnection();
      if(var1.getResponseCode() != 201 && var1.getResponseCode() != 200) {
         var0 = null;
      } else {
         var0 = convertStreamToString(var1.getInputStream());
         var1.disconnect();
      }

      return var0;
   }

   public static boolean isNetworkAvailable(Activity var0) {
      boolean var3 = false;
      ConnectivityManager var4 = (ConnectivityManager)var0.getSystemService("connectivity");
      boolean var2;
      if(var4 == null) {
         var2 = var3;
      } else {
         NetworkInfo[] var5 = var4.getAllNetworkInfo();
         var2 = var3;
         if(var5 != null) {
            int var1 = 0;

            while(true) {
               var2 = var3;
               if(var1 >= var5.length) {
                  break;
               }

               if(var5[var1].getState() == State.CONNECTED) {
                  var2 = true;
                  break;
               }

               ++var1;
            }
         }
      }

      return var2;
   }

   public int getScreenWidth() {
      Display var1 = ((WindowManager)this._context.getSystemService("window")).getDefaultDisplay();
      Point var2 = new Point();
      var2.x = var1.getWidth();
      var2.y = var1.getHeight();
      return var2.x;
   }
}
