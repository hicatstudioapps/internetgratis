package com.internetfree.util;

import java.io.Serializable;

public class Constant implements Serializable {
   public static int CATEGORYID;
   public static final String CATEGORYLIST_PRO_CATE_IMAGE = "category_image";
   public static final String CATEGORYLIST_PRO_CATE_NAME = "category_name";
   public static final String CATEGORYLIST_PRO_CATE_RATING = "rating_promedio";
   public static final String CATEGORYLIST_PRO_CID = "project_cid";
   public static final String CATEGORYLIST_PRO_CONCLUSION = "project_conclusion";
   public static final String CATEGORYLIST_PRO_C_ID = "cid";
   public static final String CATEGORYLIST_PRO_IMAGE = "project_image";
   public static final String CATEGORYLIST_PRO_INTRO = "project_intro";
   public static final String CATEGORYLIST_PRO_MATERIAL = "project_materials";
   public static final String CATEGORYLIST_PRO_NAME = "project_name";
   public static final String CATEGORYLIST_PRO_PID = "project_pid";
   public static String CATEGORYLIST_PRO_PIDD;
   public static final String CATEGORYLIST_PRO_PROCEDURE = "project_procedure";
   public static final String CATEGORYLIST_PRO_STATUS = "project_status";
   public static final String CATEGORYLIST_URL = "http://wildercs.net/wilder/app1/api.php?cat_id=";
   public static String CATEGORYNAME;
   public static final String CATEGORY_ARRAY_NAME = "Projects_App";
   public static final String CATEGORY_CID = "cid";
   public static final String CATEGORY_IMAGE = "category_image";
   public static final String CATEGORY_NAME = "category_name";
   public static final String CATEGORY_RATING = "rating_promedio";
   public static final String CATEGORY_URL = "http://wildercs.net/wilder/app1/api.php";
   public static final String SERVER_IMAGE_UPFOLDER = "http://wildercs.net/wilder/app1/images/";
   private static final long serialVersionUID = 1L;
}
