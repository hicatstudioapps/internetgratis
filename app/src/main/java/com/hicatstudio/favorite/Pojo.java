package com.internetfree.favorite;

public class Pojo {
   private String Catlist_Pro_CId;
   private String Catlist_Pro_Conclusion;
   private String Catlist_Pro_Image;
   private String Catlist_Pro_Intro;
   private String Catlist_Pro_Material;
   private String Catlist_Pro_Name;
   private String Catlist_Pro_PId;
   private String Catlist_Pro_Procedure;
   private String Catlist_Pro_Rating;
   private int id;

   public Pojo() {
   }

   public Pojo(String var1) {
      this.Catlist_Pro_PId = var1;
   }

   public Pojo(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9) {
      this.Catlist_Pro_PId = var1;
      this.Catlist_Pro_CId = var2;
      this.Catlist_Pro_Name = var3;
      this.Catlist_Pro_Image = var4;
      this.Catlist_Pro_Intro = var5;
      this.Catlist_Pro_Material = var6;
      this.Catlist_Pro_Procedure = var7;
      this.Catlist_Pro_Conclusion = var8;
      this.Catlist_Pro_Rating = var9;
   }

   public String getCatlist_Pro_CId() {
      return this.Catlist_Pro_CId;
   }

   public String getCatlist_Pro_Conclusion() {
      return this.Catlist_Pro_Conclusion;
   }

   public String getCatlist_Pro_Image() {
      return this.Catlist_Pro_Image;
   }

   public String getCatlist_Pro_Intro() {
      return this.Catlist_Pro_Intro;
   }

   public String getCatlist_Pro_Material() {
      return this.Catlist_Pro_Material;
   }

   public String getCatlist_Pro_Name() {
      return this.Catlist_Pro_Name;
   }

   public String getCatlist_Pro_PId() {
      return this.Catlist_Pro_PId;
   }

   public String getCatlist_Pro_Procedure() {
      return this.Catlist_Pro_Procedure;
   }

   public String getCatlist_Pro_Rating() {
      return this.Catlist_Pro_Rating;
   }

   public int getId() {
      return this.id;
   }

   public void setCatlist_Pro_CId(String var1) {
      this.Catlist_Pro_CId = var1;
   }

   public void setCatlist_Pro_Conclusion(String var1) {
      this.Catlist_Pro_Conclusion = var1;
   }

   public void setCatlist_Pro_Image(String var1) {
      this.Catlist_Pro_Image = var1;
   }

   public void setCatlist_Pro_Intro(String var1) {
      this.Catlist_Pro_Intro = var1;
   }

   public void setCatlist_Pro_Material(String var1) {
      this.Catlist_Pro_Material = var1;
   }

   public void setCatlist_Pro_Name(String var1) {
      this.Catlist_Pro_Name = var1;
   }

   public void setCatlist_Pro_PId(String var1) {
      this.Catlist_Pro_PId = var1;
   }

   public void setCatlist_Pro_Procedure(String var1) {
      this.Catlist_Pro_Procedure = var1;
   }

   public void setCatlist_Pro_Rating(String var1) {
      this.Catlist_Pro_Rating = var1;
   }

   public void setId(int var1) {
      this.id = var1;
   }
}
