package com.internetfree.favorite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
   private static final String DATABASE_NAME = "AddtoFav";
   private static final int DATABASE_VERSION = 1;
   private static final String KEY_ID = "id";
   private static final String KEY_PCID = "pcid";
   private static final String KEY_PCONCL = "pconcl";
   private static final String KEY_PIMAGE = "pimage";
   private static final String KEY_PINTRO = "pintro";
   private static final String KEY_PMATE = "pmate";
   private static final String KEY_PNAME = "pname";
   private static final String KEY_PPID = "ppid";
   private static final String KEY_PPROCE = "pproce";
   private static final String KEY_PRATIN = "pratin";
   private static final String TABLE_NAME = "Favorite";

   public DatabaseHandler(Context var1) {
      super(var1, "AddtoFav", (CursorFactory)null, 1);
   }

   public void AddtoFavorite(Pojo var1) {
      SQLiteDatabase var3 = this.getWritableDatabase();
      ContentValues var2 = new ContentValues();
      var2.put("ppid", var1.getCatlist_Pro_PId());
      var2.put("pcid", var1.getCatlist_Pro_CId());
      var2.put("pname", var1.getCatlist_Pro_Name());
      var2.put("pimage", var1.getCatlist_Pro_Image());
      var2.put("pintro", var1.getCatlist_Pro_Intro());
      var2.put("pmate", var1.getCatlist_Pro_Material());
      var2.put("pproce", var1.getCatlist_Pro_Procedure());
      var2.put("pconcl", var1.getCatlist_Pro_Conclusion());
      var2.put("pratin", var1.getCatlist_Pro_Rating());
      var3.insert("Favorite", (String)null, var2);
      var3.close();
   }

   public void RemoveFav(Pojo var1) {
      SQLiteDatabase var2 = this.getWritableDatabase();
      var2.delete("Favorite", "ppid = ?", new String[]{String.valueOf(var1.getCatlist_Pro_PId())});
      var2.close();
   }

   public List getAllData() {
      ArrayList var3 = new ArrayList();
      Cursor var2 = this.getWritableDatabase().rawQuery("SELECT  * FROM Favorite", (String[])null);
      if(var2.moveToFirst()) {
         do {
            Pojo var1 = new Pojo();
            var1.setId(Integer.parseInt(var2.getString(0)));
            var1.setCatlist_Pro_PId(var2.getString(1));
            var1.setCatlist_Pro_CId(var2.getString(2));
            var1.setCatlist_Pro_Name(var2.getString(3));
            var1.setCatlist_Pro_Image(var2.getString(4));
            var1.setCatlist_Pro_Intro(var2.getString(5));
            var1.setCatlist_Pro_Material(var2.getString(6));
            var1.setCatlist_Pro_Procedure(var2.getString(7));
            var1.setCatlist_Pro_Conclusion(var2.getString(8));
            var1.setCatlist_Pro_Rating(var2.getString(9));
            var3.add(var1);
         } while(var2.moveToNext());
      }

      return var3;
   }

   public List getFavRow(String var1) {
      ArrayList var2 = new ArrayList();
      var1 = "SELECT  * FROM Favorite WHERE ppid=" + var1;
      Cursor var4 = this.getWritableDatabase().rawQuery(var1, (String[])null);
      if(var4.moveToFirst()) {
         do {
            Pojo var3 = new Pojo();
            var3.setId(Integer.parseInt(var4.getString(0)));
            var3.setCatlist_Pro_PId(var4.getString(1));
            var3.setCatlist_Pro_CId(var4.getString(2));
            var3.setCatlist_Pro_Name(var4.getString(3));
            var3.setCatlist_Pro_Image(var4.getString(4));
            var3.setCatlist_Pro_Intro(var4.getString(5));
            var3.setCatlist_Pro_Material(var4.getString(6));
            var3.setCatlist_Pro_Procedure(var4.getString(7));
            var3.setCatlist_Pro_Conclusion(var4.getString(8));
            var3.setCatlist_Pro_Rating(var4.getString(9));
            var2.add(var3);
         } while(var4.moveToNext());
      }

      return var2;
   }

   public void onCreate(SQLiteDatabase var1) {
      var1.execSQL("CREATE TABLE Favorite(id INTEGER PRIMARY KEY,ppid TEXT,pcid TEXT,pname TEXT,pimage TEXT,pintro TEXT,pmate TEXT,pproce TEXT,pconcl TEXT,pratin TEXT)");
   }

   public void onUpgrade(SQLiteDatabase var1, int var2, int var3) {
      var1.execSQL("DROP TABLE IF EXISTS Favorite");
      this.onCreate(var1);
   }

   public static class DatabaseManager {

      private SQLiteDatabase db;
      DatabaseHandler dbHelper;
      private boolean isDbClosed = true;

      public void closeDatabase() {
         if(!this.isDbClosed && this.db != null) {
            this.isDbClosed = true;
            this.db.close();
            this.dbHelper.close();
         }

      }

      public void init(Context var1) {
         this.dbHelper = new DatabaseHandler(var1);
         if(this.isDbClosed) {
            this.isDbClosed = false;
            this.db = this.dbHelper.getWritableDatabase();
         }

      }

      public boolean isDatabaseClosed() {
         return this.isDbClosed;
      }
   }
}
