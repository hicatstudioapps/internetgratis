package com.internetfree.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hicatstudio.internet2016.R;
import com.internetfree.imageloader.ImageLoaderr;
import com.internetfree.item.Item_CategoryList;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

@SuppressWarnings("ResourceType")
public class CategoryList_Adapter extends ArrayAdapter {
   private Activity activity;
   public ImageLoaderr imageLoaderr;
   private List itemsCategorylist;
   private Item_CategoryList objCategorylistBean;
   private final DisplayImageOptions options;
   private int row;

   public CategoryList_Adapter(Activity var1, int var2, List var3) {
      super(var1, var2, var3);
      this.activity = var1;
      this.options = new DisplayImageOptions.Builder()
              .cacheInMemory(true)
              .showImageOnLoading(R.mipmap.ic_launcher)
              .showImageForEmptyUri(R.mipmap.ic_launcher)
              .showImageOnFail(R.mipmap.ic_launcher)
              .bitmapConfig(Bitmap.Config.RGB_565)
              .build();
      this.row = var2;
      this.itemsCategorylist = var3;
      this.imageLoaderr = new ImageLoaderr(this.activity);
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      View var5 = var2;
      CategoryList_Adapter.ViewHolder var4;
      if(var2 == null) {
         var5 = ((LayoutInflater)this.activity.getSystemService("layout_inflater")).inflate(this.row, (ViewGroup)null);
         var4 = new CategoryList_Adapter.ViewHolder();
         var5.setTag(var4);
      } else {
         var4 = (CategoryList_Adapter.ViewHolder)var2.getTag();
      }

      if(this.itemsCategorylist != null && var1 + 1 <= this.itemsCategorylist.size()) {
         this.objCategorylistBean = (Item_CategoryList)this.itemsCategorylist.get(var1);
         var4.imgv_categorylist = (ImageView)var5.findViewById(R.id.img_categorylist);
         var4.txt_categoryname = (TextView)var5.findViewById(R.id.text_catelist_imgtitle);
         var4.rt_rating = (RatingBar)var5.findViewById(R.id.ratingBar1);
         DrawableCompat.setTint(var4.rt_rating.getProgressDrawable(),Color.parseColor("#ffffff"));
//                 ((LayerDrawable) var4.rt_rating.getProgressDrawable()).getDrawable(2).setColorFilter(Color.parseColor("#ffffff"), Mode.SRC_ATOP);
         ImageLoader.getInstance().displayImage(Uri.parse("http://wildercs.net/wilder/app1/images/" + this.objCategorylistBean.getCatlist_Pro_Image().toString()).toString(), var4.imgv_categorylist, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
         });
         //this.imageLoaderr.DisplayImage("http://wildercs.net/wilder/app1/images/" + this.objCategorylistBean.getCatlist_Pro_Image().toString(), var4.imgv_categorylist);
         var4.rt_rating.setRating(Float.parseFloat(this.objCategorylistBean.getCatlist_Pro_Cate_Rating()));
         var4.txt_categoryname.setText(this.objCategorylistBean.getCatlist_Pro_Name().toString());
      }

      return var5;
   }

   public class ViewHolder {
      public ImageView imgv_categorylist;
      public RatingBar rt_rating;
      public TextView txt_categoryname;
   }
}
