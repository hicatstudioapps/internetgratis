package com.internetfree.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import com.internetfree.favorite.Pojo;
import com.internetfree.imageloader.ImageLoaderr;
import com.hicatstudio.internet2016.R;

@SuppressWarnings("ResourceType")
public class Favourite_Adapter extends ArrayAdapter {
   private Activity activity;
   private ArrayList arraylist;
   public ImageLoaderr imageLoaderr;
   private Pojo objPojoBean;
   private List pojoitem;
   private int row;
   private final DisplayImageOptions options;

   public Favourite_Adapter(Activity var1, int var2, List var3) {
      super(var1, var2, var3);
      this.activity = var1;
      this.options = new DisplayImageOptions.Builder()
              .cacheInMemory(true)
              .showImageOnLoading(R.mipmap.ic_launcher)
              .showImageForEmptyUri(R.mipmap.ic_launcher)
              .showImageOnFail(R.mipmap.ic_launcher)
              .bitmapConfig(Bitmap.Config.RGB_565)
              .build();
      this.row = var2;
      this.pojoitem = var3;
      this.imageLoaderr = new ImageLoaderr(this.activity);
      this.arraylist = new ArrayList();
      this.arraylist.addAll(this.pojoitem);
   }

   public void filter(String var1) {
      String var3 = var1.toLowerCase(Locale.getDefault());
      this.pojoitem.clear();
      if(var3.length() == 0) {
         this.pojoitem.addAll(this.arraylist);
      } else {
         Iterator var4 = this.arraylist.iterator();

         while(var4.hasNext()) {
            Pojo var2 = (Pojo)var4.next();
            if(var2.getCatlist_Pro_Name().toLowerCase(Locale.getDefault()).contains(var3)) {
               this.pojoitem.add(var2);
            }
         }
      }

      this.notifyDataSetChanged();
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      Favourite_Adapter.ViewHolder var4;
      if(var2 == null) {
         var2 = ((LayoutInflater)this.activity.getSystemService("layout_inflater")).inflate(this.row, (ViewGroup)null);
         var4 = new Favourite_Adapter.ViewHolder();
         var2.setTag(var4);
      } else {
         var4 = (Favourite_Adapter.ViewHolder)var2.getTag();
      }

      if(this.pojoitem != null && var1 + 1 <= this.pojoitem.size()) {
         this.objPojoBean = (Pojo)this.pojoitem.get(var1);
         var4.imgv_categorylist = (ImageView)var2.findViewById(R.id.picture_fav);
         var4.txt_categoryname = (TextView)var2.findViewById(R.id.text_sublist_imgtitle_fav);
         var4.rb_rating = (RatingBar)var2.findViewById(R.id.ratingBar2);
         ((LayerDrawable)var4.rb_rating.getProgressDrawable()).getDrawable(2).setColorFilter(Color.parseColor("#ffffff"), Mode.SRC_ATOP);
         ImageLoader.getInstance().displayImage(Uri.parse("http://wildercs.net/wilder/app1/images/" + this.objPojoBean.getCatlist_Pro_Image().toString()).toString(), var4.imgv_categorylist, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
         });
         this.imageLoaderr.DisplayImage("http://wildercs.net/wilder/app1/images/" + this.objPojoBean.getCatlist_Pro_Image().toString(), var4.imgv_categorylist);
         var4.rb_rating.setRating(Float.parseFloat(this.objPojoBean.getCatlist_Pro_Rating()));
         var4.txt_categoryname.setText(this.objPojoBean.getCatlist_Pro_Name().toString());
      }

      return var2;
   }

   public class ViewHolder {
      public ImageView imgv_categorylist;
      public RatingBar rb_rating;
      public TextView txt_categoryname;
   }
}
