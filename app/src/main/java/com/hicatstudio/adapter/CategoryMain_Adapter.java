package com.internetfree.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.List;
import com.internetfree.imageloader.ImageLoaderr;
import com.hicatstudio.internet2016.R;
import com.internetfree.item.Item_CategoryMain;

@SuppressWarnings("ResourceType")
public class CategoryMain_Adapter extends ArrayAdapter {
   private Activity activity;
   public ImageLoaderr imageLoaderr;
   private List itemsCategory;
   private Item_CategoryMain objCategoryBean;
   private final DisplayImageOptions options;
   private int row;

   public CategoryMain_Adapter(Activity var1, int var2, List var3) {
      super(var1, var2, var3);
      this.options = new DisplayImageOptions.Builder()
              .cacheInMemory(true)
              .showImageOnLoading(R.mipmap.ic_launcher)
              .showImageForEmptyUri(R.mipmap.ic_launcher)
              .showImageOnFail(R.mipmap.ic_launcher)
              .bitmapConfig(Bitmap.Config.RGB_565)
              .build();
      this.activity = var1;
      this.row = var2;
      this.itemsCategory = var3;
      this.imageLoaderr = new ImageLoaderr(this.activity);
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      CategoryMain_Adapter.ViewHolder var4;
      if(var2 == null) {
         var2 = ((LayoutInflater)this.activity.getSystemService("layout_inflater")).inflate(this.row, (ViewGroup)null);
         var4 = new CategoryMain_Adapter.ViewHolder();
         var2.setTag(var4);
      } else {
         var4 = (CategoryMain_Adapter.ViewHolder)var2.getTag();
      }

      if(this.itemsCategory != null && var1 + 1 <= this.itemsCategory.size()) {
         this.objCategoryBean = (Item_CategoryMain)this.itemsCategory.get(var1);
         var4.imgv_category = (ImageView)var2.findViewById(R.id.img_category);
         var4.txt_category = (TextView)var2.findViewById(R.id.text_cate_imgtitle);
//         ImageLoader.getInstance().displayImage(Uri.parse("http://wildercs.net/wilder/app1/images/" + this.objCategoryBean.getImageurl().toString()).toString(), var4.imgv_category, options, new SimpleImageLoadingListener() {
//             @Override
//             public void onLoadingStarted(String imageUri, View view) {
//             }
//
//             @Override
//             public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//             }
//
//             @Override
//             public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//             }
//         });
         var4.imgv_category.setImageDrawable(objCategoryBean.getImageurl());
         var4.txt_category.setText(this.objCategoryBean.getCategoryName().toString());
      }

      return var2;
   }

   public class ViewHolder {
      public ImageView imgv_category;
      public TextView txt_category;
   }
}
